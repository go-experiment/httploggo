build:
	go build .

clean:
	rm httploggo

run:
	go run .

install:
	go build
	sudo cp httploggo /usr/bin
