module httploggo

go 1.17

require (
	github.com/justinas/alice v1.2.0
	github.com/rs/zerolog v1.26.1
)
