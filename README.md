# httploggo

This is a verys simple go utility that is used to log http calls to it and then responding with an empty 200.
The logging is simple and prints a prettyfied Request struct



## getting started

The folliwing does help (requirements):
 - golang 1.17 + (that is what this was made of)
 - gnuMake


### build and install

```bash
make install
```

### run

#### run with default port (8080)

```bash
httploggo
```

#### run with custom port

```bash
httploggo -p 8200
```
