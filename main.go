package main

import (
	"encoding/json"
	"flag"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"net/http"
	"os"

	"github.com/justinas/alice"
)

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}


func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	portPtr := flag.String("p", "8080", "a port to listen on")
	flag.Parse()

	commonHandlers := alice.New(handlerLogging, handlerRecover)
	http.Handle("/", commonHandlers.ThenFunc(reqPrinter))

	log.Log().Msgf("listening in port %s", *portPtr)
	err := http.ListenAndServe(":"+*portPtr, nil)

	if err != nil {
		log.Log().Err(err).Msg("Could not start http server")
	}
}


// this function / handelr print what is in the req object and returns
func reqPrinter(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	reqHeadersBytes, err := json.Marshal(r.Header)
	if err != nil {
		log.Err(err).Msg("Could not Marshal Req Headers")
	}

	reqTrailerBytes, err := json.Marshal(r.Trailer)
	if err != nil {
		log.Err(err).Msg("Could not Marshal Req Trailer")
	}

	reqFormBytes, err := json.Marshal(r.Form)
	if err != nil {
		log.Err(err).Msg("Could not Marshal Req Form")
	}

	reqPostFormBytes, err := json.Marshal(r.PostForm)
	if err != nil {
		log.Err(err).Msg("Could not Marshal Req PostForm")
	}

	reqBody, err := io.ReadAll(r.Body)

	if err != nil {
		log.Err(err).Msg("could not read msq")
	}

	log.Log().
		Str("\nMethod", r.Method).
		Str("\nUrl.Scheme", r.URL.Scheme).
		Str("\nUrl.Opaque", r.URL.Opaque).
		Str("\nUrl.User.String()", r.URL.User.String()).
		Str("\nurl.Host", r.URL.Host).
		Str("\nurl.Path", r.URL.Path).
		Str("\nurl.RawPath", r.URL.RawPath).
		Bool("\nurl.ForceQuery", r.URL.ForceQuery).
		Str("\nurl.RawQuery", r.URL.RawQuery).
		Str("\nurl.Fragment", r.URL.Fragment).
		Str("\nurl.RawFragment", r.URL.RawFragment).
		Str("\nProto", r.Proto).
		Int("\nProtoMajor", r.ProtoMajor).
		Int("\nProtoMinor", r.ProtoMinor).
		Str("\nHeader", string(reqHeadersBytes)).
		Str("\nBody", string(reqBody)).
		Int64("\nContentLength", r.ContentLength).
		Strs("\nTransferEncoding", r.TransferEncoding).
		Bool("\nclose", r.Close).
		Str("\nHost", r.Host).
		Str("\nForm", string(reqFormBytes)).
		Str("\nPostForm", string(reqPostFormBytes)).
		Str("\nMultiPartForm", "not implemented").
		Str("\nTrailer", string(reqTrailerBytes)).
		Str("\nRemoteAddr", r.RemoteAddr).
		Str("\nRequestUri", r.RequestURI).
		Msg("")

}


// http hnadler for when things go wrong
func handlerRecover(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				log.Printf("panic: %+v", err)
				http.Error(w, http.StatusText(500), 500)
			}
		}()

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

// http logging handelr with time taken an return status
func handlerLogging(wrappedHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {

		lrw := NewLoggingResponseWriter(w)
		wrappedHandler.ServeHTTP(lrw, req)

		statusCode := lrw.statusCode
		//log.Printf("--> %s %s", req.Method, req.URL.Path)
		//log.Printf("<-- %d %s", statusCode, http.StatusText(statusCode))

		log.Log().
			Str("method", req.Method).
			Str("path", req.URL.Path).
			Int("status code", statusCode).
			Str("status text ", http.StatusText(statusCode)).
			Msg("")

	})
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}
